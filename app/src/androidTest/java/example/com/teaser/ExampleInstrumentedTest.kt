package example.com.teaser

import android.content.Context
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4
import example.com.teaser.repository.TeaserRepository

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*
import org.junit.Before

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {

    private var context: Context? = null

    @Before
    fun setUp() {
         context = InstrumentationRegistry.getInstrumentation().targetContext
    }

    @Test
    fun teaserData() {
        assertNotNull(TeaserRepository().getTeaserData(context!!))
    }

}
