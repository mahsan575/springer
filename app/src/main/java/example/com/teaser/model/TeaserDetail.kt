package example.com.teaser.data.model


import com.google.gson.annotations.SerializedName

data class TeaserDetail(


    @SerializedName("title")
    val title: String,
    @SerializedName("text")
    val detailText: String,
    @SerializedName("isPaid")
    val isPaid: String,
    @SerializedName("type")
    val type: String

)