package example.com.teaser.data.model


import com.google.gson.annotations.SerializedName
import example.com.teaser.data.model.TeaserDetail

data class TeaserResponse(


    @SerializedName("teasers")
    val teaserList: List<TeaserDetail>,

    @SerializedName("name")
    val name: String


)