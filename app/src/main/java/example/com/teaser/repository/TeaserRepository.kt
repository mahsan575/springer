package example.com.teaser.repository

import android.content.Context
import com.google.gson.Gson
import example.com.teaser.R
import example.com.teaser.data.model.TeaserResponse
import java.io.BufferedReader
import java.io.InputStreamReader

class TeaserRepository {
     fun getTeaserData(context: Context): TeaserResponse{
        val raw = context.resources.openRawResource(R.raw.teasers)
        val rd = BufferedReader(InputStreamReader(raw))
        val gson = Gson()
         return gson.fromJson(rd, TeaserResponse::class.java!!)
    }
}
