package example.com.teaser.ui.viewmodels

import android.app.Application
import androidx.lifecycle.*
import example.com.teaser.data.model.TeaserResponse
import example.com.teaser.repository.TeaserRepository

class TeaserViewModel(app: Application) : AndroidViewModel(app) {

    var teaserRepo = TeaserRepository()

    var teaserLiveData = MutableLiveData<TeaserResponse>()

    //var networkStatusLiveData = MutableLiveData<NetworkResponse>()

//    init {
//        DaggerAppComponent.create().inject(this)
//    }

   fun getTeasereData(): MutableLiveData<TeaserResponse> {
       teaserLiveData = MutableLiveData<TeaserResponse>()
       teaserLiveData.value = teaserRepo.getTeaserData(getApplication())
//        Scope.launch {
//            try {
//                val response =
//                    withContext(Dispatchers.IO) {
//                        teaserRepo.getTeaserData()
//                    }
//                teaserLiveData.value = response.body()
//            } catch (e: Exception) {
//                e.printStackTrace()
//            }
       // }
        return teaserLiveData
    }

}