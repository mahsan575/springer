package example.com.teaser.ui.activities

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson


abstract class BaseActivity : AppCompatActivity() {


    var gson = Gson()

    var mContext: AppCompatActivity = this
    private var parentView: View? = null
    lateinit var handler: Handler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutRes())
        //AppConstants.context = this
        handler = Handler(Looper.getMainLooper())

        init()
        onActivityLoaded()
        setEvents()
    }


    abstract fun getLayoutRes(): Int

    protected abstract fun onActivityLoaded()

    protected abstract fun init()

    protected abstract fun setEvents()


    fun makeToast(message: String) {
        Toast.makeText(mContext, message, Toast.LENGTH_LONG).show()
    }



}
