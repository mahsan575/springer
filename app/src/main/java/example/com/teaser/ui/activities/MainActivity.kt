package example.com.teaser.ui.activities

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import example.com.teaser.R
import example.com.teaser.data.model.TeaserDetail
import example.com.teaser.data.model.TeaserResponse
import example.com.teaser.ui.adapter.TeaserAdapter
import example.com.teaser.ui.viewmodels.TeaserViewModel
import kotlinx.android.synthetic.main.activity_main.*
import androidx.recyclerview.widget.DividerItemDecoration


class MainActivity : BaseActivity() {
    private lateinit var adapter: TeaserAdapter

    override fun getLayoutRes() = R.layout.activity_main


    override fun onActivityLoaded() {
    }

    override fun init() {
        val teaserViewModel = ViewModelProviders.of(this).get(TeaserViewModel::class.java)
         teaserViewModel.getTeasereData().observe(this,
            Observer<TeaserResponse> { t ->
                if (t != null) {
                    loadListMode(t.teaserList)
                    title = t.name

                }
            })
    }

    override fun setEvents() {

    }


    private fun loadListMode(teaserList: List<TeaserDetail>) {
        adapter = TeaserAdapter(this, teaserList)
        val layoutManager = LinearLayoutManager(this)
        rv.layoutManager = layoutManager
        rv.addItemDecoration(
            DividerItemDecoration(rv.context, DividerItemDecoration.VERTICAL)
        )
        rv.adapter = adapter
        adapter.notifyDataSetChanged()
    }

}
