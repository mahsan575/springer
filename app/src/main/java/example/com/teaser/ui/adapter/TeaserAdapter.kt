package example.com.teaser.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import example.com.teaser.R
import example.com.teaser.data.model.TeaserDetail
import android.widget.Toast




class TeaserAdapter(
    private val context: Context,
    private val articleList: List<TeaserDetail>?
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun onCreateViewHolder(viewGroup: ViewGroup, index: Int): RecyclerView.ViewHolder {
        var rootView =
                LayoutInflater.from(viewGroup.context).inflate(R.layout.item_list, viewGroup, false)
            ListViewHolder(rootView)


        return ListViewHolder(rootView)
    }

    override fun getItemCount(): Int {
        return articleList?.size!!
    }

    override fun onBindViewHolder(@NonNull holder: RecyclerView.ViewHolder, index: Int) {

            if (holder is ListViewHolder) {
                val viewHolder = holder
                viewHolder.tvTitle.text = articleList?.get(index)?.title
                viewHolder.tvDetail.text = articleList?.get(index)?.detailText



                viewHolder.itemView.setOnClickListener(View.OnClickListener { v ->
                    // Toast.makeText(ComplexRecyclerViewAdapter.this, "Item no: "+ position, Toast.LENGTH_LONG).show;
                    if (articleList?.get(index)?.isPaid != null) {
                        viewHolder.tvContent.text = context.getString(R.string.paid_content)
                    } else {
                        viewHolder.tvContent.text = context.getString(example.com.teaser.R.string.free_content)
                    }
                    viewHolder.tvBuySubs.visibility = View.VISIBLE

                })
            }
    }
}


class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    var tvTitle: TextView = itemView.findViewById(R.id.tvTitle) as TextView
    var tvDetail: TextView = itemView.findViewById(R.id.tvDetail) as TextView
    var tvContent: TextView = itemView.findViewById(R.id.tvContent) as TextView
    var tvBuySubs: TextView = itemView.findViewById(R.id.tvBuySubs) as TextView

}



